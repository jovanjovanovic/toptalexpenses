
class Logger
	constructor: (label = 'UNKNOWN') ->
		@label 	= label
	
	getTime: () ->
		time = new Date()
		formmated = "#{time.getDate()}-#{time.getMonth()}-#{time.getFullYear()} #{time.getHours()}:#{time.getMinutes()}:#{time.getSeconds()}"

	print: (level, toPrint) ->
		argumentsArray = Array.prototype.slice.call toPrint
		argumentsArray.splice 0, 0, @getTime(), level, "[#{@label}]"
		console.log.apply undefined, argumentsArray

	error: () ->
		#red
		@print '\x1B[31m ERROR: \x1B[39m', arguments

	debug: () ->
		# blue
		@print '\x1B[34m DEBUG: \x1B[39m', arguments
		
	warn: () ->
		# yellow
		@print '\x1B[33m WARN:  \x1B[39m', arguments

	info: () ->
		@print '\x1B[32m INFO:  \x1B[39m', arguments

module.exports = Logger