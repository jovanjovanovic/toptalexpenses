application = angular.module 'expense', []
	
	.controller 'expenseLoginController', ($scope, $http) ->
		$scope.login = () ->
			data = 
				username: $scope.username
				userpass: $scope.userpass
				remember: $scope.remember

			$http.post('/api/user/login', data)
				.success (data, status) ->
					return window.location = '/home' if data.status.code is 1000
					return $scope.errors = data.payload.error
				.error (data, status)->
					return $scope.errors.push 'An unexpected error has occured! Please try again'

	.controller 'expenseRegisterController', ($scope, $http) ->
		$scope.register = () ->
			data = 
				firstName: $scope.firstName
				lastName: $scope.lastName
				email: $scope.email
				username: $scope.username
				userpass: $scope.userpass
				agree: $scope.agree

			$http.post('/api/user', data)
				.success (data, status) ->
					return window.location = '/home' if data.status.code is 1000
					return $scope.errors = data.payload.error
				.error (data, status)->
					return $scope.errors.push 'An unexpected error has occured! Please try again'

	.controller 'fortgotPasswordController', ($scope, $http) ->
		$scope.send = () ->
			$scope.inProgress = true
			data = { email : $scope.email }

			$http.post('/api/user/forgotPassword', data)
				.success (data, status) ->
					$scope.inProgress = false
					return $scope.isSuccess = true if data.status.code is 1000
					return $scope.errors = data.payload.error
				.error (data, status)->
					$scope.inProgress = false
					return $scope.errors.push 'An unexpected error has occured! Please try again'

	.controller 'changePasswordController', ($scope, $http) ->
		$scope.send = () ->
			$scope.inProgress = true
			resetToken = window.location.pathname.replace '/changePassword/', ''
			data = 
				newPassword: $scope.newPassword
				newPasswordConfirm: $scope.newPasswordConfirm
				resetToken: resetToken

			$http.post('/api/user/changePassword', data)
				.success (data, status) ->
					$scope.inProgress = false
					return $scope.isSuccess = true if data.status.code is 1000
					return $scope.errors = data.payload.error
				.error (data, status)->
					$scope.inProgress = false
					return $scope.errors.push 'An unexpected error has occured! Please try again'
			
	.controller 'expenseViewController', ($scope, $http, $window, $element) ->

		$scope.overview = () ->
			$scope.showAmount = true
			$scope.hideButtonsAndInputs = false
			$http.post('/api/expense/all')

					.success (data, status) ->

						if data.status.code is 1000
							console.log data
							$scope.expenses = data.payload
						else
							# process errors
							console.log 'Error', data.payload

					.error (data, status)->
						console.log status, data

		$scope.getWeek = (date) ->
			
			data = {_date: date}
			$http.post('/api/expense/week', data)

				.success (data, status) ->

					if data.status.code is 1000
						console.log data
						$scope.expenses = data.payload.expenses
						$scope.sum = data.payload.expenses.sum
						$scope.average = data.payload.expenses.average
						$scope.hideButtonsAndInputs = true
						$scope.showAmount = false
					else
						# process errors
						console.log 'Error', data.payload

				.error (data, status)->
					console.log status, data

		$scope.create = () ->
			data = 
				comment: $scope.comment
				description: $scope.description
				amount: $scope.amount
				date: $scope.date

			$http.post('/api/expense/', data)

				.success (data, status) ->
					if data.status.code is 1000
						$http.post('/api/expense/all')
							.success (data, status) ->
								if data.status.code is 1000
									$scope.expenses = data.payload 
									$scope.amount = ''
									$scope.comment = ''
									$scope.description = ''
									$scope.date = ''
									$scope.errors = []
								else
									console.log 'Error', status, data
							.error (data, status)->
								console.log status, data
					else
						return $scope.errors = data.payload.error

				.error (data, status)->
					console.log status, data

		$scope.remove = (id) ->
			data = 
				_id: id
			
			$http.post('/api/expense/remove', data)

				.success (data, status) ->

					if data.status.code is 1000
						console.log data
						$http.post('/api/expense/all')

							.success (data, status) ->

								if data.status.code is 1000
									console.log data
									$scope.expenses = data.payload
								else
									# process errors
									console.log 'Error', data.payload

							.error (data, status)->
								console.log status, data
					else
						# process errors
						console.log 'Error', data.payload

				.error (data, status)->
					console.log status, data

		$scope.logout = () ->
			$http.get('/api/user/logout')
				.success (data, status) ->
					#console.log data, status
					window.location = '/' if data.status.code is 1000
				.error (data, status) ->
					# process errors
					console.log 'Error', data.payload