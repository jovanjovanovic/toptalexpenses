should    = require 'should'
request   = require 'supertest'
mongoose  = require 'mongoose'

# connect to database and delete collections
describe 'DATABASE', ()->

	this.timeout 10000

	it 'should connect to database', (done) ->
		mongoose.connect 'mongodb://localhost/expenses'
		mongoose.connection.on 'error', (error) -> done error
		mongoose.connection.on 'open',  () -> done()

	it 'should delete collection users', (done) ->
		mongoose.connection.db.dropCollection 'users', (err) ->
			return done err if err? 
			return done()

	it 'should delete collection tokens', (done) ->
		mongoose.connection.db.dropCollection 'tokens', (err) ->
			return done err if err? 
			return done()


# REST api
describe 'REST', () ->
	
	baseUrl = 'http://localhost:8000'
	Credentials = credentials:{}
	expenseData = 
			credentials: {}
			id: ""
			amount: 23
			comment: "Test1"
			date: "2014-08-01"
			description: "Test2"
			time: "02:02:00"
	removalData = 
			credentials: {}
			_id: ""
	weekData = 
			credentials: {}
			_date: ""
	# user tests
	describe 'User', () ->

		userData =
			username: 'jovanjovanovic'
			userpass: 'test123'
			firstName: 'Jovan'
			lastName: 'Jovanovic'
			email: 'jocca1985@gmail.com'
			agree: true
		
		
		it 'should create new user', (done) ->

			request(baseUrl)
				.post('/api/user')
				.send(userData)
				.end (err, res) -> 
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1000
					done()

		
		it 'should fail to create user with existing username', (done) ->

			userData.email = 'example@test.com'
			request(baseUrl)
				.post('/api/user')
				.send(userData)
				.end (err, res) -> 
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1002
					done()


		it 'should fail to create user with existing email', (done) ->

			userData.username = 'somebody';
			userData.email    = 'jocca1985@gmail.com';
			request(baseUrl)
				.post('/api/user')
				.send(userData)
				.end (err, res) ->
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1002
					done()


		it 'should login user with email, check user credentials and returns user info', (done) ->

			userData =
				username: 'jocca1985@gmail.com'
				userpass: 'test123'

			request(baseUrl)
				.post('/api/user/login')
				.send(userData)
				.end (err, res) ->
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1000
					expenseData.credentials = res.body.payload.credentials
					Credentials.credentials = res.body.payload.credentials
					removalData.credentials = res.body.payload.credentials
					weekData.credentials = res.body.payload.credentials
					done()

		it 'should fail to login user with wrong username', (done) ->

			userData =
				username: 'jocca1985'
				userpass: 'test123'

			request(baseUrl)
				.post('/api/user/login')
				.send(userData)
				.end (err, res) ->
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1001
					done()

		it 'should fail to login user with wrong password', (done) ->

			userData =
				username: 'jocca1985'
				userpass: 'test123da'

			request(baseUrl)
				.post('/api/user/login')
				.send(userData)
				.end (err, res) ->
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1001
					done()

	describe 'Expense', () ->


		it 'should create expense', (done) ->
			console.log expenseData
			request(baseUrl)
				.post('/api/expense/')
				.send(expenseData)
				.end (err, res) -> 
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1000
					done()	

		it 'should display expenses', (done) ->
			request(baseUrl)
				.post('/api/expense/all')
				.send(Credentials)
				.end (err, res) -> 
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1000
					removalData._id = res.body.payload.expenses[0].id
					weekData._date = res.body.payload.expenses[0].date
					done()

		it 'should remove expense', (done) ->
			request(baseUrl)
				.post('/api/expense/remove')
				.send(removalData)
				.end (err, res) -> 
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1000
					done()

		it 'should display week report', (done) ->
			#console.log weekData
			request(baseUrl)
				.post('/api/expense/week')
				.send(weekData)
				.end (err, res) -> 
					throw err if err?
					res.body.should.have.property 'status'
					res.body.status.should.have.property 'code'
					res.body.status.code.should.equal 1000
					done()