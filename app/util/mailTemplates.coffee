module.exports = MailTemplates =
	
	getActivationEmail: (user, resend) ->
		return log.debug  'User not defined' if not user

		mailData = 
			to: 	 	user.email
			subject: 	'Activatie your ExpensesTracker account'
			template: 	if resend then 'activationResend' else 'activation'
			data:
				user: user
				link: "http://glooky.com/activateAccount/#{user.activationToken}"

	getResetPasswordEmail: (user) ->
		return log.debug  'User not defined' if not user

		mailData = 
			to: 	 	user.email
			subject: 	'ExpensesTracker password change request'
			template: 	'forgotPassword'
			data:
				changePasswordLink: "#{hostName}/changePassword/#{user.resetToken}"

	getChangedPasswordEmail: () ->

		mailData = 
			to: 	 	user.email
			subject: 	'ExpensesTracker password changed'
			template: 	'changePassword'
			data: 		{}