alerts = 

	# user registration form alerts
	ERR_FIRST_NAME_EMPTY: 		'First name is required field.'
	ERR_FIRST_NAME_LONG: 		'First name is too long (max 50 characters).'
	ERR_LAST_NAME_EMPTY: 		'Last name is required field.'
	ERR_LAST_NAME_LONG: 		'Last name is too long (max 50 characters).'
	ERR_EMAIL_EMPTY: 			'Email is required field.'
	ERR_EMAIL_WRONG: 			'Wrong email address.'
	ERR_EMAIL_LONG: 			'Email is too long (max 50 characters).'
	ERR_PASSWORD_EMPTY: 		'Password is required field.'
	ERR_PASSWORD_LENGTH: 		'Password should be between  6 and 30 characters long.'
	ERR_USERNAME_EMPTY: 		'Username is required field.'
	ERR_USERNAME_LENGTH: 		'Username should be between  6 and 30 characters long.'
	ERR_USERNAME_EMAIL_TAKEN: 	'Username or email is already in use.'
	ERR_TERMS_NOT_ACCEPTED: 	'You have to accept terms of use.'

	# user change password 
	ERR_PASSWORD_NOT_SAME: 		'Entered passwords are not same.'
	ERR_RESET_TOKEN_EMPTY:		'Please check if your reset password link is correct, it seems you have a wrong link.'
	ERR_RESET_TOKEN_WRONG:		'Please check if your reset password link is correct, it seems you have a wrong link.'
	# user loggin form alerts
	ERR_USERNAME_PASSWORD_WRONG:'Username/E-mail or password are not correct! Please try again.'
	ERR_EMAIL_DOES_NOT_EXISTS:  'We could not find the user with given email'

	# expenses error
	ERR_EXPENSE_AMOUNT_EMPTY:		'Amount is required field.'
	ERR_EXPENSE_AMOUNT_NOT_NUMBER: 	'Amount must be a number value.'
	ERR_EXPENSE_DESCRIPTION_EMPTY:	'Description is required field.'


	# general alerts
	ERR_UNEXPECTED: 			'An unexpected error has occured.'


module.exports = (identifier) ->
	if alerts[identifier]? 
		return alerts[identifier]
	else 
		return 'General error' 