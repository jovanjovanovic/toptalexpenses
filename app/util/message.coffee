
module.exports = {

    EmailError: {
        title: 'Email error'
        text: 'The email that you send us is not correct.'
        buttonLink: '/forgotPassword'
        buttonLabel: 'Go back'
    }

	EmailNotExists: {
        title: 'Email does not exists'
        text: 'The email that you send us is not in our database please check if you have typed the right email address'
        buttonLink: '/forgotPassword'
	    buttonLabel: 'Go back'
    }

    EmailForgotPasswordSent: {
        title: 'Email notification sent'
        text: 'We have sent you email with link to change password.'
        buttonLink: '/'
	    buttonLabel: 'Go home'
    }

    ChangePasswordError: {
        title: 'Error changing password'
        text: 'An error has occured changing your password. please check that you have correct link and you have typed your new password correctly.'
        buttonLink: 'javascript:history.go(-1)'
        buttonLabel: 'Go back'
    }

    ChangePasswordSuccess: {
        title: 'Password changed'
        text: 'Your password has been changed successfully please try to log in with new password.'
        buttonLink: '/'
        buttonLabel: 'Go home to login'  
    }

    ActivationError: {
        title: 'Account activation error'
        text: 'An error occured during account activation, please check that you have a correct link or login to see if your account is already activated.'
        buttonLink: '/'
        buttonLabel: 'Go home'  
    }

    ActivationSuccess: {
        title: 'Account activated'
        text: 'Your account is activated successfully. Thank you.'
        buttonLink: '/'
        buttonLabel: 'Go home'  
    }

    ServerError: {
        title: 'ServerError'
        text: 'Ops... Something went wrong, we will figure out what have heppened.'
        buttonLink: 'javascript:history.go(-1)'
        buttonLabel: 'Go back'
    }
}