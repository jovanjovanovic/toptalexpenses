
# logger
log = new Logger 'EXPENSE_SERVICE'

# dependencies
Expense 	 = include modules.model.Expense
ServiceError = include modules.error.ServiceError
moment	 	 = include 'moment'

module.exports = ExpenseService = 

	# Save user into database
	save: (expense, callback) ->
		return log.debug  'No callback defined' if not callback?		
		return log.debug  'No expense defined' if not expense?
		console.log expense
		expense = new Expense expense

		expense.save (error, expense) ->
			if error?
				log.debug  'Unexpected error saving expense'
				return callback new ServiceError 'ERR_UNEXPECTED', error

			log.debug  'Expense saved successfully', expense
			return callback null, expense

	remove: (id, callback) ->
		return log.debug  'No callback defined' if not callback?		
		return log.debug  'No expense defined' if not id?

		Expense.remove {_id: id}, (error)->
			if error?
				log.debug  'Unexpected error deleting expense'
				return callback new ServiceError 'ERR_UNEXPECTED', error

			log.debug  'Expense deleted successfully'
			return callback null, id
		
		

	findAll: (user, callback) ->
		return log.debug  'No callback defined' if not callback?		
		return log.debug  'No user defined' if not user?
		
		query = Expense.find({userId: user._id})

		query.exec (error, expenses) ->

			# check for error
			if error?
				log.debug  'Error occurred on finding expenses', error
				return callback new ServiceError 'ERR_UNEXPECTED', error

			formatedExpenses = []
			for expense, i in expenses
				formatedExpenses.push expense.getData()
			return callback null, formatedExpenses

	getWeek: (date, user, callback) ->
		return log.debug  'No callback defined' if not callback?		
		return log.debug  'No user defined' if not user?

		#Testing while not build datepicker
		weekDate = moment(date)
		daysToMonday = 0 - (1 - weekDate.isoWeekday())
		weekBegin = weekDate.clone().subtract(daysToMonday, 'days')
		query = Expense.find({userId: user._id, creationDate:{$gte: weekBegin, $lte: weekBegin.clone().add(6, 'days')}})
		console.log query
		#end of testing
		query.exec (error, expenses) ->

			# check for error
			if error?
				log.debug  'Error occurred on finding expenses', error
				return callback new ServiceError 'ERR_UNEXPECTED', error
			result = {}
			formatedExpenses = []
			amountSum = 0
			for expense, i in expenses
				formatedExpenses.push expense.getData()
				amountSum = amountSum + expense.getData().amount
			result = {expenses: formatedExpenses, sum: amountSum, average: amountSum/expenses.length}
			console.log amountSum, amountSum/expenses.length
			return callback null, result
	