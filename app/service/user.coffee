# logger
log = new Logger 'USER_SERVICE'

# dependencies
User 		 = include modules.model.User
helper		 = include modules.tools.helper
ServiceError = include modules.error.ServiceError
tokenService = include modules.service.token

mailClient 	 	= include modules.tools.mailClient
mailTemplates	= include modules.util.mailTemplates


module.exports = UserService = 

	# Save user into database
	save: (user, callback) ->
		return log.debug  'No callback defined' if not callback?		
		return log.debug  'No user defined' if not user?

		user.save (error, user) ->
			if error?
				log.debug  'Unexpected error saving user'
				return callback new ServiceError 'ERR_UNEXPECTED', error

			log.debug  'User saved successfully', user
			return callback null, user


	# Fetch user by given field/value
	getUserByField: (field, value, callback)->
		return log.debug  'No callback defined' if not callback?
		return log.debug  'No field/value defined' if not field? or not value?

		query = User.find().where(field).equals(value);
		query.exec (error, user) ->
			if error?
				log.debug  'Error executing query to find user by', field, 'equals to', value, error
				return callback new ServiceError 'ERR_UNEXPECTED', error

			if user.length != 1
				log.debug  'User not found or too many users', user
				return callback new ServiceError 'ERR_UNEXPECTED', error

			log.debug  'User fetched from database', user
			return callback null, user[0]


	# Fetch user by email
	getUserByEmail: (email, callback) ->
		return log.debug  'No email defined' if not email?
		UserService.getUserByField 'email', email, callback

	# Fetch user by username
	getUserByUsername: (username, callback) ->
		return log.debug  'No username defined' if not username?
		UserService.getUserByField 'username', username, callback

	# Fetch user by accessId
	getUserByAccessId: (accessId, callback) ->
		return log.debug  'No accessId defined' if not accessId?
		UserService.getUserByField 'accessId', accessId, callback

	# Fetch user by reset token
	getUserByResetToken: (resetToken, callback) ->
		return log.debug  'No resetToken defined' if not resetToken?
		UserService.getUserByField 'resetToken', resetToken, callback

	# Fetch user by activation token
	getUserByActivationToken: (activationToken, callback) ->
		return log.debug  'No activationToken defined' if not activationToken?
		UserService.getUserByField 'activationToken', activationToken, callback
	
	# Fetch user by field/value pair
	getUserByFieldWithLoginTokens: (field, value, callback) ->
		return log.debug  'No callback defined' if not callback?
		UserService.getUserByField field, value, (error, user) ->
			return callback error, user if error? or not user?

			user.loginTokens = []
			tokenService.getTokensByUserId user._id, (error, tokens) ->
				if not error?
					user.loginTokens = tokens

				return callback error, user

	# Check if username/email exists and crate user
	create: (userData, callback) ->
		return log.debug 'No callback defined' if not callback?

		# find user with given username or email
		query = User.find().or([{username: userData.username}, {email: userData.email}])
		query.exec (error, user) ->

			# check for error
			if error?
				log.debug  'Error occurred creating user', error
				return callback new ServiceError 'ERR_UNEXPECTED', error

			# check if username/email already exists
			return callback new ServiceError 'ERR_USERNAME_EMAIL_TAKEN' if user.length > 0

			# create slated password and activation token
			userData.password = helper.saltAndHash userData.password
			userData.activationToken = helper.generateHash()
			UserService.save new User(userData), callback

	
	# Change users password
	changePassword: (resetToken, newPassword, callback) ->
		return log.debug  'No callback defined' if not callback?
		return log.debug  'No reset token/password defined' if not resetToken? or not newPassword?

		UserService.getUserByResetToken resetToken, (error, user) ->
			console.log error
			return callback new ServiceError 'ERR_UNEXPECTED' if error? and error.ServiceErrorInfo?
			return callback new ServiceError 'ERR_RESET_TOKEN_EMPTY' if not user?

			user.password = helper.saltAndHash newPassword
			user.resetToken = ''
			
			UserService.save user, callback


	# Login user according to given credentials and return user
	login: (username, password, callback) ->
		return log.debug  'No callback defined' if not callback?
		return log.debug  'No reset username/password defined' if not username? or not password?

		query = User.find().or([{username: username}, {email: username}])
		query.exec (error, users) ->

			# check for errors
			if error?
				log.debug  'Error executing login query to find users', error
				return callback new ServiceError 'ERR_UNEXPECTED', error

			authUser = null

			if users.length > 0
				for user in users
					if helper.validatePassword password, user.password
						authUser = user
						break

			return callback null, authUser if authUser?

			log.debug  'Wrong username or password', username, password
			return callback new ServiceError 'ERR_USERNAME_PASSWORD_WRONG'


	# Login user using username and token
	loginWithKeys: (accessId, accessKey, callback) ->
		return log.debug  'No callback defined' if not callback?
		return log.debug  'No reset username/token defined' if not accessId? or not accessKey?

		UserService.getUserByFieldWithLoginTokens 'accessId', accessId, (error, user) ->
			return callback error if error?

			for tokenData in user.loginTokens
				return callback null, user if accessKey is tokenData.token

			log.debug 'User access key not found', accessKey
			return callback()


	# Save user login token
	createAccessKey: (user, callback) ->
		return log.debug  'No callback defined' if not callback?
		return log.debug  'No reset user/token defined' if not user?

		tokenService.create user._id, (error, tokenData) ->
			return callback error if error?

			callback null, tokenData.token


	# Remove users login token
	deleteAccessKey: (user, accessKey) ->
		return log.debug  'No user/token defined' if not user.username? or not accessKey?

		UserService.getUserByFieldWithLoginTokens 'username', user.username, (error, user) ->
			return log.debug 'Error fetching user to delete token', error if error?

			for tokenData in user.loginTokens
				return tokenData.remove() if accessKey is tokenData.token

	# Activate users account
	activateAccount: (activationToken, callback) ->
		return log.debug  'No callback defined' if not callback?
		return log.debug  'No activationToken defined' if not activationToken?

		UserService.getUserByActivationToken activationToken, (error, user) ->
			return callback error if error?
			return callback() if not user?

			log.debug 'User account status', user.accountStatus

			if user.accountStatus != 0
				log.debug  'Account has already been activated'
				return callback new ServiceError 'ERR_ACCOUNT_ALREADY_ACTIVATED'
				
			user.accountStatus = 1
			UserService.save user, callback
	
	# Creates new reset token for user
	resetPasswordToken: (email, callback) ->
		return log.debug  'No callback defined' if not callback?
		return log.debug  'No email defined' if not email?

		UserService.getUserByEmail email, (error, user) ->
			return callback new ServiceError 'ERR_UNEXPECTED' if error? and error.ServiceErrorInfo?
			return callback new ServiceError 'ERR_EMAIL_DOES_NOT_EXISTS' if not user?

			user.resetToken = helper.generateHash()
			UserService.save user, callback

	# Send or resend activation email to user
	sendEmailActivation: (user, resend = false, callback) ->
		return log.debug  'User not defined' if not user

		mailData = mailTemplates.getActivationEmail user, resend
		mailClient.send mailData, (error) ->
			return callback new ServiceError 'ERR_UNEXPECTED', error if callback? and error?
			return callback() if callback?

	sendEmailForgotPassword: (user, callback) ->
		return log.debug  'User not defined' if not user

		mailData = mailTemplates.getResetPasswordEmail user
		mailClient.send mailData, (error) ->
			return callback new ServiceError 'ERR_UNEXPECTED', error if callback? and error?
			return callback() if callback?