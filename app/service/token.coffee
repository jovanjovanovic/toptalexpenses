
# logger
log = new Logger 'TOKEN_SERVICE'

# dependencies
Token 		 = include modules.model.Token
helper		 = include modules.tools.helper
ServiceError = include modules.error.ServiceError

module.exports = TokenService =

	create: (userId, callback) ->
		token = new Token {
			userId: userId
			token: helper.generateHash()
		}

		token.save (error, token) ->
			if error?
				log.debug 'Error saving token', error
				return callback new ServiceError 'ERR_UNEXPECTED', error

			log.debug 'User token created successfully', token
			return callback null, token

	getTokensByUserId: (userId, callback) ->
		query = Token.find().where('userId').equals userId
		query.exec (error, tokens) ->
			if error?
				log.debug 'Error reading tokens by user id', error
				return callback new ServiceError 'ERR_UNEXPECTED', error

			return callback null, tokens