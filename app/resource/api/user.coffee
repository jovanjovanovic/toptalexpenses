# logger
log = new Logger 'API_USER'

# dependencies
userService  = include modules.service.user
HttpResponse = include modules.resource.HttpResponse

helper      = include modules.tools.helper
validator   = include modules.tools.validator

# User resource definition
module.exports = UserResource =


	# Register user 
	create: (request, response) ->
		# validate user data
		error = validator.userRegister request
		return response.json new HttpResponse.BadRequest error if error?

		# when data is valid, extract user data from request
		userData = 
			firstName: 		request.body.firstName
			lastName:		request.body.lastName
			email:			request.body.email
			username:		request.body.username
			password:		request.body.userpass
			accessId: 		helper.generateHash()
			accountStatus: 	1

		# call service to create user
		userService.create userData, (error, user) ->
			return response.json new HttpResponse.Error if error?

			request.login user
			return response.json new HttpResponse.Success
			
			###
			send activation mail to user is currently off
			user sendActivationMail from user service and remove
			account status in order to have conformation mails
			###


	# Login user
	login: (request, response) ->
		username = request.body.username
		password = request.body.userpass
		remember = request.body.remember

		error = validator.userLogin request
		return response.json new HttpResponse.BadRequest error if error?

		userService.login username, password, (error, user) ->
			log.debug error
			return response.json new HttpResponse.BadRequest error.data if error?
			
			request.login user

			log.debug 'User successfully logged in', user

			userService.createAccessKey user, (error, accessKey) ->
				return response.json new HttpResponse.Error if error?
				
				if remember? and remember is yes
					
					expire = 
						expires: new Date Date.now() + 2 * 604800000
						path: '/'
					
					response.cookie 'accessId', user.accessId, expire
					response.cookie 'accessKey', accessKey, expire
				

				credentials = 
					accessId: user.accessId
					accessKey: accessKey

				return response.json new HttpResponse.Success { credentials: credentials }

	logout: (request, response) ->

		return new HttpResponse.Success if not request.isAuthenticated()

		accessId  = request.cookies.accessId
		accessKey = request.cookies.accessKey
		user 	  = request.session.user

		console.log 'Logging out user', user

		userService.deleteAccessKey user, request.cookies.accessKey

		request.logout()

		response.clearCookie 'accessId'
		response.clearCookie 'accessKey'
		response.json new HttpResponse.Success

	profile: (request, response) ->
		request.verifyUser (error, user) ->
			userService.getUserByAccessId user.accessId, (error, user) ->
				return response.json new HttpResponse.Error if error?
				return response.json new HttpResponse.Success { profile: user }

	forgotPassword: (request, response) ->

		error = validator.userEmail request
		return response.json new HttpResponse.BadRequest error if error?

		email = request.body.email
		userService.resetPasswordToken email, (error, user) ->
			return response.json new HttpResponse.BadRequest error.data if not user?

			userService.sendEmailForgotPassword user, (error) ->
				return response.json new HttpResponse.Success
	
	changePassword: (request, response) ->

		error = validator.userNewPassword request
		return response.json new HttpResponse.BadRequest error if error?

		resetToken  = request.body.resetToken
		newPassword	= request.body.newPassword

		userService.changePassword resetToken, newPassword, (error, user) ->
			return response.json new HttpResponse.BadRequest error.data if error?
			return response.json new HttpResponse.Success
		