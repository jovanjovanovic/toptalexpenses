
#logger
log = new Logger "API_EXPENSES"

# dependencies
expenseService  = include modules.service.expense
HttpResponse 	= include modules.resource.HttpResponse
ServiceError 	= include modules.error.ServiceError

helper     		= include modules.tools.helper
validator   	= include modules.tools.validator

# User resource definition
module.exports = ExpenseResource =


	# Register user 
	create: (request, response) ->
		# validate if user is logged in
		request.verifyUser (err, user) ->
			return response.json new HttpResponse.BadRequest new ServiceError if err? || !user?

			# validate expense data
			error = validator.expenseCreate request
			return response.json new HttpResponse.BadRequest error if error?

			# when data is valid, extract expense data from request
			expenseData = 
				description:	request.body.description
				amount:			request.body.amount
				comment:		request.body.comment
				userId:			user._id

			expenseData.creationDate = request.body.date if request.body.date?
				
				
			#call service to create user
			expenseService.save expenseData, (error, expense) ->
				return response.json new HttpResponse.Error if error?
				return response.json new HttpResponse.Success

	findAll: (request, response) ->
		request.verifyUser (err, user) ->
			return response.json new HttpResponse.BadRequest new ServiceError if err? || !user?

			expenses = expenseService.findAll user, (error, expenses) ->
				return response.json new HttpResponse.Error if error?
				return response.json new HttpResponse.Success {expenses: expenses}

	remove: (request, response) ->
		request.verifyUser (err, user) ->
			return response.json new HttpResponse.BadRequest new ServiceError if err? || !user?

			expenseService.remove request.body._id, (error, expenses) ->
				return response.json new HttpResponse.Error if error?
				return response.json new HttpResponse.Success

	getWeek: (request, response) ->
		request.verifyUser (err, user) ->
			return response.json new HttpResponse.BadRequest new ServiceError if err? || !user?

			expenses = expenseService.getWeek request.body._date, user, (error, expenses) ->
				return response.json new HttpResponse.Error if error?
				return response.json new HttpResponse.Success {expenses: expenses}


