
# logger
log = new Logger 'PAGE_HOME'

# index page controller
module.exports = (request, response) ->
     
		return response.render 'common/error404' if not request.isAuthenticated()
		return response.render 'home'