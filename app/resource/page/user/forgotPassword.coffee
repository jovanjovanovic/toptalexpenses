
# logger
log = new Logger 'PAGE_FORGOT_PASSWORD'

# register page controller
module.exports = (request, response) ->
	return response.redirect '/home' if request.isAuthenticated()
	return response.render 'user/forgotPassword'