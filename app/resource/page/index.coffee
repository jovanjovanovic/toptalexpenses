
# logger
log = new Logger 'PAGE_INDEX'

# index page controller
module.exports = (request, response) ->
	return response.redirect '/home' if request.isAuthenticated()
	return response.render 'index'