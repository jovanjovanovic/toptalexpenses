
class HttpResponseError
	constructor: (error) ->
		@status = 
			code: 1002
			tag: 'ERROR'
		@payload =
			error: error

class HttpResponseBadRequest
	constructor: (error) ->
		@status = 
			code: 1001
			tag: 'BAD_REQUEST'
		@payload =
			error: error

class HttpResponseSuccess
	constructor: (@payload) ->
		@status = 
			code: 1000
			tag: 'SUCCESS'


module.exports.Error 		= HttpResponseError
module.exports.BadRequest 	= HttpResponseBadRequest
module.exports.Success 		= HttpResponseSuccess