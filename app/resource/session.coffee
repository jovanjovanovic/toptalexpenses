
# logger
log = new Logger 'SESSION_MIDDLEWARE'

# dependencies
userService = include modules.service.user

# session middleware definition
module.exports = SessionMiddleware =
	
	
	###
	Initialize functions on request
	###
	initialize: (request, response, next)->
		request.isAuthenticated = () -> 
			request.session.user?

		request.login = (user) ->
			return callback new ServiceError 'ERR_NO_USER', 'No user defined' if not user?
			
			log.debug 'Saving user into session'
			request.session.user = user

		request.logout = () -> 
			delete request.session.user if request.session.user?
			
		###
		Check for keys/credentials in request body
		and return response if they are correct
		###
		request.checkCredentials = (callback) ->
			accessId  = request.body.credentials.accessId
			accessKey = request.body.credentials.accessKey

			userService.loginWithKeys accessId, accessKey, (error, user) ->
				return callback error if error? or not user?
				callback null, user

		request.verifyUser = (callback)->
			return callback null, request.session.user if request.isAuthenticated()
			request.checkCredentials callback

		next()


	###
	Check cookie if there are api keys, if api keys are correct,
	login user and start session		
	###
	checkCookie: (request, response, next) ->
		log.debug 'Cookie content', request.cookies

		return next() if !request.cookies.accessId? or !request.cookies.accessKey? or request.isAuthenticated()

		# authenticate user via cookie api keys
		accessId 	= request.cookies.accessId
		accessKey 	= request.cookies.accessKey

		userService.loginWithKeys accessId, accessKey, (error, user) ->
			return next() if error? or not user?
			request.login user	
			next()
