
# dependencies
mongoose = include 'mongoose'

# User DAO definition
schema = {
	# user data
	email: 		{ type: String, lowercase: true, trim: true }
	username:	{ type: String, lowercase: true, trim: true }
	firstName: String
	lastName: String
	password: String
	accessId: String
	accountStatus: { type: Number, default: 0 }
	activationToken: String
	resetToken: String

	updatedAt: { type: Date, default: Date.now }
	createdAt: { type: Date, default: Date.now }
}

# create user schema
UserSchema = mongoose.Schema schema

# User instance methods
UserSchema.methods.getDisplayName = () ->
	"#{@firstName} #{@lastName}"

# User visible data
UserSchema.methods.getData = () ->
	data = 
		firstName: 		@firstName
		lastName: 		@lastName
		username:		@username
		accountStatus:	@accountStatus
		displayName: 	"#{@firstName} #{@lastName}"
		createdAt: 		@createdAt.getTime()

module.exports = mongoose.model 'User', UserSchema