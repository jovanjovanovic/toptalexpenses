# dependencies
mongoose = include 'mongoose'

# Token dao definition
schema = {
	token: String
	userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
	updatedAt: { type: Date, default: Date.now }
	createdAt: { type: Date, default: Date.now }
}

# create token schema
TokenSchema = mongoose.Schema schema
TokenSchema.path('updatedAt').index({ expires: 60*60*24*30 });

TokenModel 	= mongoose.model 'Token', TokenSchema

module.exports = TokenModel