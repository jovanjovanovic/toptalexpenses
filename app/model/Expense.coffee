
# dependencies
mongoose = include 'mongoose'
moment	 = include 'moment'

# Expense DAO definition
schema = {
	# expense data
	creationDate: 			{ type: Date, default: Date.now }
	description: 	String
	amount: 		Number
	comment:	 	String
	userId: 		{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }
}

# create user schema
ExpensesSchema = mongoose.Schema schema

# User expense data
ExpensesSchema.methods.getData = () ->
	data = 
		date: 			moment(@creationDate.getTime()).format "YYYY-MM-DD"
		time:			moment(@creationDate.getTime()).format "HH:mm:ss"
		description:	@description
		amount: 		@amount
		comment:		@comment
		id: 			@_id 
		

module.exports = mongoose.model 'Expense', ExpensesSchema