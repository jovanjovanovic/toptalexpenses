alert = include modules.util.alert

class ServiceError
	constructor: (tag, @ServiceErrorInfo = null) ->
		@data = []
		@data.push { tag: tag, message: alert tag }

module.exports = ServiceError