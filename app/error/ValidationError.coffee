
# dependencies
alert = include modules.util.alert

class ValidationError
	constructor: (@field, @tag, @value) ->
		@message = alert @tag

module.exports = ValidationError