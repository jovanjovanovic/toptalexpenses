# defining routes

api 	= include modules.resource.api
page 	= include modules.resource.page

# this method routes all requests
module.exports = (app) ->

	# PAGES
	# ---------------------------------------------------------------------
	app.get '/', 							page.index
	app.get '/home',						page.user.home
	app.get '/register', 					page.user.register
	app.get '/forgotPassword',				page.user.forgotPassword
	app.get '/changePassword/:resetToken',	page.user.changePassword


	# REST api user
	# ---------------------------------------------------------------------
	
	## api user
	app.post '/api/user',       			api.user.create
	app.post '/api/user/login',   			api.user.login
	app.get  '/api/user/logout',			api.user.logout
	app.post '/api/user/profile', 		 	api.user.profile
	app.post '/api/user/forgotPassword',	api.user.forgotPassword
	app.post '/api/user/changePassword',	api.user.changePassword
	
	## api expense
	app.post '/api/expense/',				api.expense.create
	app.post '/api/expense/all',			api.expense.findAll
	app.post '/api/expense/week',			api.expense.getWeek
	app.post '/api/expense/remove',			api.expense.remove