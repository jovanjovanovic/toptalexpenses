
# logger
log = new Logger 'START'

# constants
PORT_HTTP 	= 8000
PORT_HTTPS	= 443

# node modules
path = include 'path'

# app third party modules 
mongoose 		 		= include 'mongoose'
express 		 		= include 'express'
expressValidator 		= include 'express-validator'
expressSession 	 		= include 'express-session'
expressLogger 	 		= include 'morgan'
expressBodyParser 		= include 'body-parser'
expressCookieParser 	= include 'cookie-parser'
expressMethodOverride 	= include 'method-override'
expressCoffee 			= include 'coffee-middleware'
expressLess 	 		= include 'less-middleware'

# app modules
router 	  	= include modules.router
validator 	= include modules.tools.validator
session 	= include modules.resource.session

# app static content paths
pathLess	= path.join __dirname, '../client/less'
pathCoffee	= path.join __dirname, '../client/coffee' 
pathStatic	= path.join __dirname, '../public'
pathView 	= path.join __dirname, '../views'

# create express app and configure it
app = express()

app.set 'views', pathView
app.set 'view engine', 'jade'

app.use expressLogger 'combined'
app.use expressBodyParser.json()
app.use expressMethodOverride 'X-HTTP-Method-Override'
app.use expressSession { secret: 'some secret here', saveUninitialized: true, resave: true }
app.use expressCookieParser 'some secret here'
app.use expressValidator errorFormatter: validator.errorFormatter

app.use expressLess pathLess, { 
	dest: pathStatic,
	preprocess:
    	path: (pathname, req) ->pathname.replace '/style/', '/'
}

app.use expressCoffee {
	src: pathCoffee,
	compress: true,
	prefix: '/js/',
	encodeSrc: false,
	debug: true
}

app.use express.static pathStatic

app.use session.initialize
app.use session.checkCookie

router app

# open database connection
mongoose.connect 'mongodb://localhost/expenses'

dbConnection = mongoose.connection
dbConnection.on 'error', (error) ->
	log.error 'Error opening database connection', error
dbConnection.once 'open', ->
	log.info 'Connection to database opened successfully'

	# run http server
	server = app.listen PORT_HTTP, ()->
		log.info 'Http server started on port', server.address().port