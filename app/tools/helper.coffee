
# dependencies
crypto = include 'crypto'

module.exports = Helper = 

	generateSalt: () ->
		set  = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ'
		salt = ''

		for i in [1..10]
			p = Math.floor Math.random() * set.length
			salt += set[p]

		return salt

	generateHash: () ->
		Helper.generateSalt() + Helper.md5 "aa#{Helper.getTimestamp()}"

	md5: (string) ->
		crypto.createHash('md5').update(string).digest('hex');

	saltAndHash: (pass, callback) ->
		salt = Helper.generateSalt()
		return salt + Helper.md5 pass + salt

	validatePassword: (plainPass, hashedPass) ->
		salt = hashedPass.substr 0, 10
		validHash = salt + Helper.md5 plainPass + salt
		return validHash is hashedPass

	getTimestamp: () ->
		date = new Date()
		date.getTime();