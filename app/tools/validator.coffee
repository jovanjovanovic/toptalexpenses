# logging label
log = new Logger 'VALIDATION'

# dependencies
ValidationError = include modules.error.ValidationError

# validation module definition
module.exports = 
	
	# format errors for request validation
	errorFormatter: (param, tag, value) ->

		namespace = param.split '.'
		root	  = namespace.shift()
		formParam = root

		while namespace.length
			formParam += "[#{namespace.shift()}]"

		return new ValidationError formParam, tag, value

	# check users data
	userRegister: (request) ->
	    request.assert('firstName', 'ERR_FIRST_NAME_EMPTY').notEmpty()
	    request.assert('firstName', 'ERR_FIRST_NAME_LONG').len 0, 50
	    request.assert('lastName',  'ERR_LAST_NAME_EMPTY').notEmpty()
	    request.assert('lastName',  'ERR_LAST_NAME_LONG').len 0, 50 
	    request.assert('email',     'ERR_EMAIL_EMPTY').notEmpty()
	    request.assert('email',     'ERR_EMAIL_WRONG').isEmail()
	    request.assert('email',     'ERR_EMAIL_LONG').len 0, 50
	    request.assert('username',  'ERR_USERNAME_EMPTY').notEmpty()
	    request.assert('username',  'ERR_USERNAME_LENGTH').len 6, 30
	    request.assert('userpass',  'ERR_PASSWORD_EMPTY').notEmpty()
	    request.assert('userpass',  'ERR_PASSWORD_LENGTH').len 6, 30
	    request.assert('agree',     'ERR_TERMS_NOT_ACCEPTED').equals 'true'

	    errors = request.validationErrors() 
	    log.debug 'Could not register user, user data not valid', errors if errors?
	    return errors


	userLogin: (request) ->
		request.assert('username',     'ERR_USERNAME_EMPTY').notEmpty()
		request.assert('userpass',     'ERR_PASSWORD_EMPTY').notEmpty()

		errors = request.validationErrors() 
		log.debug 'User credentials missing', errors if errors?
		return errors

	# check users email
	userEmail: (request) ->
		request.assert('email', 'ERR_EMAIL_EMPTY').notEmpty()
		request.assert('email', 'ERR_EMAIL_WRONG').isEmail()
		
		errors = request.validationErrors() 
		log.debug 'User email not valid', errors if errors?
		return errors

	# check users password
	userNewPassword: (request) ->
		request.assert('newPassword', 'ERR_PASSWORD_EMPTY').notEmpty()
		request.assert('newPassword', 'ERR_PASSWORD_LENGTH').len 6, 30
		request.assert('newPassword', 'ERR_PASSWORD_NOT_SAME').equals request.body.newPasswordConfirm
		request.assert('resetToken',  'ERR_RESET_TOKEN_EMPTY').notEmpty()

		errors = request.validationErrors() 
		log.debug 'User new password not valid', errors if errors?
		return errors

	expenseCreate: (request) ->
		request.assert('amount',		'ERR_EXPENSE_AMOUNT_EMPTY').notEmpty()
		request.assert('amount', 		'ERR_EXPENSE_AMOUNT_NOT_NUMBER').isFloat()
		request.assert('description', 	'ERR_EXPENSE_DESCRIPTION_EMPTY').notEmpty()

		errors = request.validationErrors() 
		log.debug 'User new password not valid', errors if errors?
		return errors