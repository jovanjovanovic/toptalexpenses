###
Mail client - this module repersent a wrapper for 
nodemailer mail client, also uses jade templating 
engine to reneder mail content
###

# logging label
log = new Logger 'MAIL_CLIENT'

# dependencies
jade 		= include 'jade'
path 		= include 'path'
fs 			= include 'fs'
clone 		= include 'clone'
nodemailer 	= include 'nodemailer'

# templates path
templateDir = path.join __dirname, '../../views/mailTemplates/'

# empty email tamplate
emptyEmail =
    from: 	 "Expenses Tracker <no-reply@example.com>"
    to: 	 "no-reply@example.com"
    subject: "Empty email"
    html:  	 "<b>Hello</b>"
    text:	 ""
    headers: "{ 'X-Laziness-level': 1000 }"

# configure smtp
smtpTransportData = 
    service: "Gmail"
    auth:
        user: "expensetrackertoptal@gmail.com"
        pass: "test442213"

smtpTransport = nodemailer.createTransport smtpTransportData

# send 
module.exports.send = (emailData, callback) ->
	return log.debug 'No callback defined' if not callback?
	return log.debug 'No email data defined' if not emailData?

	templatePath 	= "#{templateDir}#{emailData.template}.jade"
	file 			= fs.readFileSync templatePath, 'utf8'
	compile			= jade.compile file, filename: templatePath

	emailToSend 		= clone emptyEmail
	emailToSend.to 		= emailData.to
	emailToSend.subject = emailData.subject
	emailToSend.html	= compile emailData.data

	smtpTransport.sendMail emailToSend, (error, info) ->
		
		smtpTransport.close()

		if error?
			log.debug 'Error sending email:', error
			return callback error

		log.debug 'Mail sent:', info.response
		return callback()