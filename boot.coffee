# application details
# root directory of app and startpoint
APP_DIR 		= './app'
APP_START_POINT = 'start'
HOST_NAME 		= 'http://localhost:8000'
LOG_LEVEL 		= 'DEBUG'
MODULE_EXTENSION = '.coffee'


# make logger global
GLOBAL.Logger = require './util/Logger'

# logger
log = new Logger 'BOOT'

# dependencies
fs 	 = require 'fs'
path = require 'path'

# Boot loader definition
BootLoader = 

	loadFolderTree: (filename) ->
		stats = fs.lstatSync filename
		info = 
			path: path.join __dirname, filename
			name: path.basename filename

		if not stats.isDirectory()
			info.type = 'file'
			return info

		info.type = 'folder'
		info.children = fs.readdirSync(filename).map (child) ->
			BootLoader.loadFolderTree "#{filename}/#{child}"

		return info


	parseFolderTree: (item) ->
		modules = {}
		if item.children?
			for child in item.children
				if child.type is 'folder'
					modules[child.name] = BootLoader.parseFolderTree child
					continue

				#if endsWith child.name, MODULE_EXTENSION

				if child.name.indexOf(MODULE_EXTENSION, child.name.length - MODULE_EXTENSION.length) isnt -1
					name = child.name.substr(0, child.name.lastIndexOf('.'))
					modules[name] = path: child.path
					log.debug 'Loaded module', child.path
				else
					log.warn "Not #{MODULE_EXTENSION} module", child.path

		return modules


	include: (module) ->
		return require module if typeof module is 'string'
		return require module.path if module.path?

		loadedModule = {}
		for name, mod of module
			loadedModule[name] = include mod
		return loadedModule


	boot: (appDir, appStartPoint, hostName, logLevel) ->
		
		GLOBAL.logLevel 	= logLevel
		GLOBAL.hostName 	= hostName
		
		# load folder tree
		log.debug 'Parsing module tree', 'testing'
		folderTree = BootLoader.loadFolderTree appDir

		GLOBAL.modules 		= BootLoader.parseFolderTree folderTree
		GLOBAL.include 		= BootLoader.include

		log.debug 'Starting application'

		if modules[appStartPoint]?
			startPath = modules[appStartPoint].path
			delete modules[appStartPoint]
			require startPath


BootLoader.boot APP_DIR, APP_START_POINT, HOST_NAME, LOG_LEVEL